const express = require("express");
const mongoose = require("mongoose");

// configuration
const app = express(); // server
const port = 5002; // port

app.use(express.json());
app.use(express.urlencoded({extended:true})); // for methods and content type


// Mongo DB connection and will save to s35-activity
mongoose.connect("mongodb+srv://iandealday:Brevity01@cluster0.qmugmsf.mongodb.net/s35-activity?retryWrites=true&w=majority",
    {
        useNewUrlParser : true,
        useUnifiedTopology : true
    }
);


// Set notification for connection success or failure in MongoDB
let db = mongoose.connection;

db.on("error", console.error.bind(console, "connection error"));

db.on("open", () => console.log("Connected to MongoDB!"));



// 1.Create a User schema.
//Schema
const accountSchema = new mongoose.Schema({ 
    userName : String,
    password : String
});


// 2. Create a User model.

const Account = mongoose.model("Account", accountSchema);

app.post('/signup', (req,res) => {

    Account.findOne({userName : req.body.userName}, (err,result) => {

        isNewUser = result === null; // && result.userName === req.body.userName; 
        
        if (!isNewUser) {
            return res.send("User is already exist. Kindly input another one.");
        }
        
        isPasswordValid = req.body.password.length >= 8;
        
        if (!isPasswordValid) {
            return res.send("Invalid password. Password must be atleast 8 characters.")
        }
    
        let newAccount = new Account ({ 
            userName : req.body.userName,
            password : req.body.password
        })
       

        saveToDatabase(newAccount, res);      
    })
});



app.listen(port, () => console.log(`Server running at port ${port}!`));


function saveToDatabase(newAccount, res) {
    newAccount.save((saveErr, savedAccount) => {

        if (saveErr) {
            return console.error(saveErr);
        }

        return res.status(201).send("New Account created");
    });
}
